/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.conversion;

/**
 *
 * @author Luis Silva
 */
public class Conversion {
    private String numeroBinario;
    private String numeroDecimal;  
    private String numBinarioUno;
    private String numBinarioDos;
    
    public int binarioADecimal(String binario){      
        int suma = 0;
        double multi = 0;
        int exponente = 0;
        for(int x= binario.length(); x > 0; x--){
            String numBinario = binario.substring(x-1, x);           
            double numBi = Double.parseDouble(numBinario);            
            multi = Math.pow(2,exponente);            
            suma += numBi * multi;
            exponente++;     
        }
        return suma;
    }   
    public int decimalABinario(String decimal){        
        int numDecimal = Integer.parseInt(decimal);
        String conversion = Integer.toBinaryString(numDecimal);
        int binario = Integer.parseInt(conversion);
        return binario;
    }
    public int sumaNumBinarios(String numBinario1, String numBinario2){
        int suma = binarioADecimal(numBinario1) + binarioADecimal(numBinario2);        
        String sumaBinario = Integer.toBinaryString(suma);
        int numBinario = Integer.parseInt(sumaBinario);
        return numBinario;
    }
    public void convesiones(){
        System.out.println("Número en Binario "+numeroBinario+" a Decimal = "+binarioADecimal(numeroBinario));
        System.out.println("Número en Decimal "+numeroDecimal+" a Binario = "+decimalABinario(numeroDecimal));
        System.out.println("\tSuma de binarios:\nNúmero Binario uno =  "+numBinarioUno+" en decimal = "+binarioADecimal(numBinarioUno)
                                           +"\nNúmero Binario dos =  "+numBinarioDos+" en decimal = "+binarioADecimal(numBinarioDos)
                                           +"\nsuma = "+numBinarioUno+" + "+numBinarioDos+" = "+sumaNumBinarios(numBinarioUno, numBinarioDos)+" en decimal = "+binarioADecimal(sumaNumBinarios(numBinarioUno, numBinarioDos)+""));
        
    }            
    public void setNumeroBinario(String numeroBinario) {
        this.numeroBinario = numeroBinario;
    }

    public void setNumeroDecimal(String numeroDecimal) {
        this.numeroDecimal = numeroDecimal;
    }

    public void setNumBinarioUno(String numBinarioUno) {
        this.numBinarioUno = numBinarioUno;
    }

    public void setNumBinarioDos(String numBinarioDos) {
        this.numBinarioDos = numBinarioDos;
    }
    public static void main(String[] args){
        Conversion con = new Conversion();
        //Número binario a convertir a decimal
        con.setNumeroBinario("11100");
        //Número decimal a convertir a Binario
        con.setNumeroDecimal("25");
        
        //Primer número binario para la suma
        con.setNumBinarioUno("1101");
        //Segundo número binario para la suma
        con.setNumBinarioDos("1111");
        
        //método que ejecuta todos los métodos
        con.convesiones();
        
    }
    
            
}
