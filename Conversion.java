/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.conversion;

/**
 *
 * @author Luis Silva
 */
public class Conversion {
    private String numeroBinario;
    private String numeroDecimal;  
    private String numBinarioUno;
    private String numBinarioDos;
    
    public int binarioADecimal(String binario){      
        int suma = 0;
        double multi = 0;
        int exponente = 0;
        for(int x= binario.length(); x > 0; x--){
            String numBinario = binario.substring(x-1, x);           
            double numBi = Double.parseDouble(numBinario);            
            multi = Math.pow(2,exponente);            
            suma += numBi * multi;
            exponente++;     
        }
        return suma;
    }   
    public int decimalABinario(String decimal){        
        int numDecimal = Integer.parseInt(decimal);
        String conversion = Integer.toBinaryString(numDecimal);
        int binario = Integer.parseInt(conversion);
        return binario;
    }   
    public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
    }
    
    public void conversiones(){
        System.out.println("Número en Binario "+numeroBinario+" a Decimal = "+binarioADecimal(numeroBinario));
        System.out.println("Número en Decimal "+numeroDecimal+" a Binario = "+decimalABinario(numeroDecimal));
        System.out.println("\tSuma de binarios:\nNúmero Binario uno =  "+numBinarioUno+" en decimal = "+binarioADecimal(numBinarioUno)
                                           +"\nNúmero Binario dos =  "+numBinarioDos+" en decimal = "+binarioADecimal(numBinarioDos)
                                           +"\nsuma = "+numBinarioUno+" + "+numBinarioDos+" = "+sumaDeBinarios(numBinarioUno, numBinarioDos)+" en decimal = "+binarioADecimal(sumaDeBinarios(numBinarioUno, numBinarioDos)+""));
        
    }            
    public void setNumeroBinario(String numeroBinario) {
        this.numeroBinario = numeroBinario;
    }

    public void setNumeroDecimal(String numeroDecimal) {
        this.numeroDecimal = numeroDecimal;
    }

    public void setNumBinarioUno(String numBinarioUno) {
        this.numBinarioUno = numBinarioUno;
    }

    public void setNumBinarioDos(String numBinarioDos) {
        this.numBinarioDos = numBinarioDos;
    }
    public static void main(String[] args){
        Conversion con = new Conversion();
        //Número binario a convertir a decimal
        con.setNumeroBinario("11100");
        //Número decimal a convertir a Binario
        con.setNumeroDecimal("25");
        
        //Primer número binario para la suma
        con.setNumBinarioUno("1111");
        //Segundo número binario para la suma
        con.setNumBinarioDos("1110");
        
        //método que ejecuta todos los métodos
        con.conversiones();
       
    }  
            
}
